import XCTest

protocol LevelEditor {
    func show() -> String
}

class ConcreteComponent: LevelEditor {
    func show() -> String {
        return "ConcreteComponent"
    }
}

class Decorator: LevelEditor {

    private var component: LevelEditor

    init(_ component: LevelEditor) {
        self.component = component
    }

    func show() -> String {
        return component.show()
    }
}

class Terrain: Decorator {

    override func show() -> String {
        return "Terrain selected" + super.show() + ")"
    }
}

class Clima: Decorator {

    override func show() -> String {
        return "Clima(" + super.show() + ")"
    }
}

class Client {
    // ...
    static func someClientCode(component: LevelEditor) {
        print("Result: " + component.show())
    }
    // ...
}

class DecoratorConceptual: XCTestCase {

    func testDecoratorConceptual() {
        print("Client: I've got a simple component")
        let simple = ConcreteComponent()
        Client.someClientCode(component: simple)
        let terrain = Terrain(simple)
        let clima = clima(decorator1)
        print("\nClient: Now I've got a decorated component")
        Client.someClientCode(component: decorator2)
    }
}
