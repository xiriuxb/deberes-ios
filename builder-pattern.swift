import XCTest

/// The Builder interface specifies methods for creating stages for a game
protocol Builder {

    func setDimensions()
    func setFloor()
    func setChests()
}

class CreateStage: Builder {

    private var stage = Stage()

    func reset() {
        stage = Stage()
    }

    func setDimensions() {
        stage.add(part: "23*35")
    }

    func setFloor() {
        stage.add(part: "stone")
    }

    func setChests() {
        stage.add(part: "3")
    }

    func retrieveProduct() -> Stage {
        let result = self.stage
        reset()
        return result
    }
}

class CreateMap: Builder {

    private var map = Map()

    func reset() {
        map = Map()
    }

    func setDimensions() {
        map.add(part: "23*35")
    }

    func setFloor() {
        map.add(part: "Stone texture")
    }

    func setChests() {
        map.add(part: "chest texture for Map")
    }

    func retrieveProduct() -> Map {
        let result = self.map
        reset()
        return result
    }
}

class Director {

    private var builder: Builder?

    func update(builder: Builder) {
        self.builder = builder
    }

    func buildPlainStage() {
        builder?.setDimensions()
    }

    func buildStoneStage() {
        builder?.setDimensions()
        builder?.setFloor()
        builder?.setChests()
    }
}

class Stage {

    private var parts = [String]()

    func add(part: String) {
        self.parts.append(part)
    }

    func listParts() -> String {
        return "Product parts: " + parts.joined(separator: ", ") + "\n"
    }
}

class Map {

    private var parts = [String]()

    func add(part: String) {
        self.parts.append(part)
    }

    func listParts() -> String {
        return "Product parts: " + parts.joined(separator: ", ") + "\n"
    }
}

class Client {
    // ...
    static func someClientCode(director: Director) {
        let builder = CreateStage()
        director.update(builder: builder)
        
        print("Standard basic product:")
        director.buildPlainStage()
        print(builder.retrieveProduct().listParts())

        print("Standard full featured product:")
        director.buildStoneStage()
        print(builder.retrieveProduct().listParts())
    }
    // ...
}

class BuilderConceptual: XCTestCase {

    func testBuilderConceptual() {
        var director = Director();
        Client.someClientCode(director: director)
    }
}

BuilderConceptual().testBuilderConceptual()