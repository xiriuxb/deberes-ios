import UIKit
import Foundation
import XCTest

protocol Scene {
    func createEnemies() -> Enemy
    func render() -> String
}

extension Scene {
    func render() -> String {
        let product = createEnemies()
        return "Creator: The same creator's code has just worked with " + product.walk()
    }
}

class Scene1: Scene {
    public func createEnemies() -> Enemy {
        return Slime()
    }
}

class Scene2: Scene {
    public func createEnemies() -> Enemy {
        return Wolf()
    }
}

/// The Product protocol declares the operations that all concrete products must
/// implement.
protocol Enemy {

    func walk() -> String
    func attack() -> String
}

/// Concrete Products provide various implementations of the Product protocol.
class Slime: Enemy {

    func walk() -> String {
        return "Slime walk"
    }
    
    func attack() -> String {
        return "Slime attack"
    }
}

class Wolf: Enemy {

    func walk() -> String {
        return "Wolf walk"
    }
    
    func attack() -> String {
        return "Wolf attack"
    }
}


class Client {
    // ...
    static func someClientCode(creator: Scene) {
        print("Client: I'm not aware of the creator's class, but it still works.\n"
            + creator.render())
            
    }

}

class FactoryMethodConceptual: XCTestCase {

    func testFactoryMethodConceptual() {

        //print("App: Launched with the ConcreteCreator1.")
        //Client.someClientCode(creator: Scene1())

        print("\nApp: Launched with the ConcreteCreator2.");
        Client.someClientCode(creator: Scene1())
    }
}

FactoryMethodConceptual().testFactoryMethodConceptual();
